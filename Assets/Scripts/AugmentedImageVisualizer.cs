
namespace Scripts
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using GoogleARCore;
    using GoogleARCoreInternal;
    using UnityEngine;

    /// <summary>
    /// Uses 4 frame corner objects to visualize an AugmentedImage.
    /// </summary>
    public class AugmentedImageVisualizer : MonoBehaviour
    {
        
        public AugmentedImage Image;

        public GameObject obj;
        public Vector3 translation;

        
        public void Update()
        {

            if (Image == null || Image.TrackingState != TrackingState.Tracking)
            {
                obj.SetActive(false);
                
                return;
            }

            obj.transform.localPosition = translation;
            obj.SetActive(true);

            /*
            if (Image == null || Image.TrackingState != TrackingState.Tracking)
            {
                interieur.SetActive(false);
                businesscard.SetActive(false);
                sockel.SetActive(false);
                return;
            }

            interieur.transform.localPosition = (2* Vector3.left) + (2 * Vector3.back);


            interieur.SetActive(true);
            businesscard.SetActive(true);
            sockel.SetActive(true);
            */
        }
    }
}
