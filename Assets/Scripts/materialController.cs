﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class materialController : MonoBehaviour {

    [SerializeField]
    GameObject button;
    [SerializeField]
    Material[] materials;
    [SerializeField]
    GameObject[] items;

    int counterFloor;

    bool buffer;
    int counter;
    
	void Start () {
        buffer = true;
        counter = 0;
        counterFloor = 0;
    }
    
    void changeMaterial()
    {
        if (counter < materials.Length - 1)
            counter++;
        else
            counter = 0;
        for (int i = 0; i<items.Length; i++)
            items[i].GetComponent<Renderer>().material = materials[counter];
    }
   
    void Update () {
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                Ray ray = Camera.current.ScreenPointToRay(touch.position);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject == button)
                        changeMaterial();
                    
                }
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray_mouse = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray_mouse, out hit))
            {
                if (hit.collider.gameObject == button)
                    changeMaterial();
            }
        }
    }
}